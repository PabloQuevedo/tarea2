/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tareaseis;

/**
 *
 * @author PABLO
 */
public class ucatro {
    char arr[];
    int top;
    
    ucatro(){
        this.arr = new char[5];
        this.top = -1;
    }
    //empty -> si esta vacio o tiene datos la pila
    public boolean isEmpty(){
        return (top == -1);
    }
    //push  -> para insertar datos a la pila
    public void push(char element){
        top++;// aumente +1
        if(top < arr.length){//5 < 5
            arr[top] = element;
        }else{
            //arr -> [s,q,e,r,t]
            char temp[] = new char[arr.length+5];
            //temp -> [ , , , , , , , , , ]
            for(int i = 0; i<arr.length; i++){//arr -> [s,q,e,r,t]
                temp[i] = arr[i];
            }
            //temp -> [s,q,e,r,t, , , , , ]
            arr = temp; //arr => [s,q,e,r,t, , , , , ]
            arr[top] = element;//arr => [s,q,e,r,t,newelement , , , , ]
        }
    }
    //peek  -> para mostrar el dato que este en la cima de la pila o el ultimo dato
    public char peek(){
        return arr[top];
    }
    //pop   -> para desapilar lo que este en la pila o sacar lo datos
    public char pop(){
        if(!isEmpty()){
            int temptop = top;
            top--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '-';
        }
    }
    
}
   


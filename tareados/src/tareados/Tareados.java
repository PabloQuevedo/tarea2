package tareados;
import javax.swing.JOptionPane;
/**
 *
 * @author PABLO Quevedo
 */
public class Tareados {

       public static void  main(String[] args){
        String inputlimit,inputtype,inputdata;
        inputlimit = JOptionPane.showInputDialog("Introduzca un limite de datos");//pidiendo datos por pantalla
        inputtype = JOptionPane.showInputDialog("Que pila desea usar :\n -estatica \n -dinamica");
        int aux = 1;
        int limit = Integer.parseInt(inputlimit);
        String result = "";
        switch(inputtype){
            case "dinamica":
                stackDinamic pd = new stackDinamic();
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        pd.push(Integer.parseInt(inputdata));
                    aux++;
                }while(aux <= limit);
                stackDinamic resultpd = dinamicNumbers(pd);//<- llamando a funcion
                while(!resultpd.isEmpty()){
                    result = result +'-'+resultpd.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila dinamica:\n"+result);
            break;
            case "estatica":
                stackStatic ps = new stackStatic(limit);
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        ps.push(Integer.parseInt(inputdata));
                    aux++;
                }while(aux <= limit);//solo estamos llenando datos dinamicamente por pantalla
                
                stackStatic resultps = stackNumbers(ps,limit);//<- llamando a funcion
                while(!resultps.isEmpty()){
                    result = result +'-'+resultps.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila estatica:\n"+result);
            break;
            default:
                
                JOptionPane.showMessageDialog(null, "Escoja un valor valido");
            break;
            
            
        }
    }
    
    //crear una funcion de tipo "stackStatic" y el nombre que le estoy dando es: "stackNumbers"
    //que recive como parametros una variable de tipo "stackStatic"->ps y la otra variable]\
    // es de tipo entero que es el limite que pedimos al principio
    public static stackDinamic dinamicNumbers(stackDinamic pd){
            stackDinamic p1d = new stackDinamic();
            stackDinamic p2d = new stackDinamic();
            //System.out.println(ps.peek());
            //->ps  = [1,7,7,2,2,3]
            while(!pd.isEmpty()){       //iterando hasta que la pila esta vacia
                int temp = pd.pop();//temp = 3
                //->ps  = [1,7,7,2,2]
                int count = 1;         /// contador para contar los numeros que se repiten
                while(!pd.isEmpty()){
                    int temp2 = pd.pop();//temp2 = 2
                    //ps->[1,7,7,2]
                    /// 2    ==   3
                    if(temp2 == temp){//comparamos si son iguales o no
                        count++; // aumentamos el contador
                    }else{
                        p2d.push(temp2);
                        //p2->[2]
                    }
                }
                //[2,...]
                if(count == 1){
                    p1d.push(temp);
                    //p1->[3]
                }
                while(!p2d.isEmpty()){//vaciando datos que sean comparado para volver a llenar la pila principal
                    pd.push(p2d.pop());
                }
            }
            return p1d;
            /*while(!p1d.isEmpty()){
                System.out.println(p1d.pop());
            }*/
    }
    
    public static stackStatic stackNumbers(stackStatic ps, int limit){
        stackStatic p1 = new stackStatic(limit);
        stackStatic p2 = new stackStatic(limit);
        //System.out.println(ps.peek());
        //->ps  = [1,7,7,2,2,3]
        while(!ps.isEmpty()){       //iterando hasta que la pila esta vacia
            int temp = ps.pop();//temp = 3
            //->ps  = [1,7,7,2,2]
            int count = 1;         /// contador para contar los numeros que se repiten
            while(!ps.isEmpty()){
                int temp2 = ps.pop();//temp2 = 2
                //ps->[1,7,7,2]
                /// 2    ==   3
                if(temp2 == temp){//comparamos si son iguales o no
                    count++; // aumentamos el contador
                }else{
                    p2.push(temp2);
                    //p2->[2]
                }
            }
            //[2,...]
            if(count == 1){
                p1.push(temp);
                //p1->[3]
            }
            while(!p2.isEmpty()){//vaciando datos que sean comparado para volver a llenar la pila principal
                ps.push(p2.pop());
            }
        }
        return p1;
        /*while(!p1.isEmpty()){
            System.out.println(p1.pop());
        }*/
    }
    
    
    
}

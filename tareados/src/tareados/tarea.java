/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tareados;
import java.util.ArrayList;
/**
 *
 * @author PABLO
 */
public class tarea {
    
    private ArrayList<Integer> stack = new ArrayList();//stack va ser un array list de tipo objecto
    
    public Boolean isEmpty(){//para verificar si mi pila esta vacia o tiene datos
        return stack.isEmpty();//true o false
    }
    
    public void push(int o){//introducir datos
        stack.add(o);
    }
    
    public int peek(){
        if(!(stack.isEmpty())){
            return stack.get(stack.size()-1);//array.length = x.size
        }else{
            return -1;
        }
    }
    
    public int pop(){
        if(!(stack.isEmpty())){
            int o = stack.get(stack.size()-1);
            stack.remove(stack.size()-1);
            return o;
        }else{
            return -1;
        }
    }
}

